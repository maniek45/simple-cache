package com.example.cache;


import com.codahale.metrics.health.HealthCheck;
import com.example.cache.mgmt.AutoCacheEvictionManager;
import com.example.cache.resource.StringCacheResource;
import com.example.cache.service.AutoCacheEvictionService;
import com.example.cache.service.ExpiringCacheService;
import io.dropwizard.Application;
import io.dropwizard.setup.Environment;

import java.util.concurrent.Executors;

public class CacheApplication extends Application<CacheConfiguration> {

    public static void main(String[] args) throws Exception {
        new CacheApplication().run(args);
    }

    @Override
    public String getName() {
        return "cache-app";
    }

    @Override
    public void run(CacheConfiguration configuration, Environment environment) throws Exception {
        ExpiringCacheService<String> cacheStorageService = new ExpiringCacheService<>(configuration
                .getEntryExpirationTimeoutInMs(), System::currentTimeMillis);

        environment.lifecycle().manage(new AutoCacheEvictionManager(new AutoCacheEvictionService(cacheStorageService,
                configuration.getAutoEvictionIntervalInMs(), Executors::newSingleThreadScheduledExecutor)));

        environment.jersey().register(new StringCacheResource(cacheStorageService));

        environment.healthChecks().register("fakeHealthCheck", new HealthCheck() {
            @Override
            protected Result check() throws Exception {
                return Result.healthy();
            }
        });
    }
}
