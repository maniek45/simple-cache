package com.example.cache;

import io.dropwizard.Configuration;

import javax.validation.constraints.Min;
import java.util.concurrent.TimeUnit;

public class CacheConfiguration extends Configuration {

    @Min(1)
    private long entryExpirationTimeoutInMs = TimeUnit.HOURS.toMillis(1);

    @Min(1)
    private long autoEvictionIntervalInMs = TimeUnit.MINUTES.toMillis(5);

    public CacheConfiguration(long entryExpirationTimeoutInMs, long autoEvictionIntervalInMs) {
        this.entryExpirationTimeoutInMs = entryExpirationTimeoutInMs;
        this.autoEvictionIntervalInMs = autoEvictionIntervalInMs;
    }

    public CacheConfiguration() {
    }

    public long getEntryExpirationTimeoutInMs() {
        return entryExpirationTimeoutInMs;
    }

    public long getAutoEvictionIntervalInMs() {
        return autoEvictionIntervalInMs;
    }

}
