package com.example.cache.mgmt;

import com.example.cache.service.AutoCacheEvictionService;
import io.dropwizard.lifecycle.Managed;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AutoCacheEvictionManager implements Managed {

    private final Logger logger = LoggerFactory.getLogger(AutoCacheEvictionManager.class);

    private final AutoCacheEvictionService autoCacheEvictionService;

    public AutoCacheEvictionManager(AutoCacheEvictionService autoCacheEvictionService) {
        this.autoCacheEvictionService = autoCacheEvictionService;
    }

    @Override
    public void start() throws Exception {
        logger.info("start cache eviction manager");
        autoCacheEvictionService.start();
    }

    @Override
    public void stop() throws Exception {
        logger.info("stop cache eviction manager");
        autoCacheEvictionService.stop();
    }
}
