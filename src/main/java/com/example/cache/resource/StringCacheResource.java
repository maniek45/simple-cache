package com.example.cache.resource;

import com.example.cache.service.CacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

@Path("cache")
@Produces(MediaType.APPLICATION_JSON)
public class StringCacheResource {
    private static final String KEY_PARAM = "key";

    private static final String KEY_PATH_PARAM = "{" + KEY_PARAM + "}";

    private final Logger logger = LoggerFactory.getLogger(StringCacheResource.class);

    private final CacheService<String> cacheService;

    public StringCacheResource(CacheService<String> cacheService) {
        this.cacheService = cacheService;
    }

    @GET
    @Path(KEY_PATH_PARAM)
    public Response read(@PathParam(KEY_PARAM) String key) {
        logger.info("cache read: {}", key);

        String value = cacheService.get(key);
        logger.info("cache value: {}", value);

        return value != null ? Response.ok(value).build() : Response.status(Status.NOT_FOUND).build();
    }

    @POST
    @Path(KEY_PATH_PARAM)
    public Response add(@PathParam(KEY_PARAM) String key, String value) {
        logger.info("cache add: {} = {}", key, value);

        boolean isNew = cacheService.put(key, value);

        return isNew ? Response.created(UriBuilder.fromResource(StringCacheResource.class).path(KEY_PATH_PARAM).build
                (key)).build() : Response.ok().build();
    }

    @DELETE
    @Path(KEY_PATH_PARAM)
    public Response remove(@PathParam(KEY_PARAM) String key) {
        logger.info("cache remove: {}", key);

        boolean isDeleted = cacheService.delete(key);

        return isDeleted ? Response.ok().build() : Response.status(Status.NOT_FOUND).build();
    }

    @DELETE
    public Response invalidateAll() {
        logger.info("cache invalidate");

        cacheService.invalidateAll();

        return Response.ok().build();
    }

}