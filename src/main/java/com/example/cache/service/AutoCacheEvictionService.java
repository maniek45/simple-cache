package com.example.cache.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

public class AutoCacheEvictionService {

    private final Logger logger = LoggerFactory.getLogger(AutoCacheEvictionService.class);

    private final EvictionService evictionService;

    private final long evictionIntervalInMs;

    private final Supplier<ScheduledExecutorService> scheduledExecutorServiceSupplier;

    private volatile ScheduledExecutorService executorService;

    public AutoCacheEvictionService(EvictionService evictionService, long evictionIntervalInMs,
                                    Supplier<ScheduledExecutorService> scheduledExecutorServiceSupplier) {
        this.evictionService = evictionService;
        this.evictionIntervalInMs = evictionIntervalInMs;
        this.scheduledExecutorServiceSupplier = scheduledExecutorServiceSupplier;
    }

    public void start() {
        if (executorService != null) {
            throw new IllegalStateException("cache eviction service has been already started");
        }
        logger.info("starting cache eviction service");
        executorService = scheduledExecutorServiceSupplier.get();
        executorService.scheduleAtFixedRate(evictionService::evict, evictionIntervalInMs, evictionIntervalInMs,
                TimeUnit.MILLISECONDS);
    }

    public void stop() {
        if (executorService != null) {
            executorService.shutdown();
            executorService = null;
        }
    }
}
