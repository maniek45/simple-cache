package com.example.cache.service;

public interface CacheService<T> {

    boolean put(String key, T value);

    boolean delete(String key);

    T get(String key);

    void invalidateAll();
}
