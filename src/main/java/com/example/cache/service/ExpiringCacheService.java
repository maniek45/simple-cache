package com.example.cache.service;

import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.LongSupplier;

public class ExpiringCacheService<T> implements CacheService<T>, EvictionService {

    private final Map<String, ValueElement<T>> cache = new ConcurrentHashMap<>();

    private final long expirationTimeoutInMs;

    private final LongSupplier currentDateSupplier;

    public ExpiringCacheService(long expirationTimeoutInMs, LongSupplier currentDateSupplier) {
        this.expirationTimeoutInMs = expirationTimeoutInMs;
        this.currentDateSupplier = currentDateSupplier;
    }

    public boolean put(String key, T value) {
        long expirationTsInMs = currentDateSupplier.getAsLong() + expirationTimeoutInMs;
        return cache.put(key, new ValueElement<>(value, expirationTsInMs)) == null;
    }

    public T get(String key) {
        ValueElement<T> value = cache.get(key);
        long currentDateTs = currentDateSupplier.getAsLong();
        return value != null && !value.isExpired(currentDateTs) ? value.getValue() : null;
    }

    public boolean delete(String key) {
        return cache.remove(key) != null;
    }

    @Override
    public void evict() {
        Iterator<ValueElement<T>> valueIterator = cache.values().iterator();
        long currentDateTs = currentDateSupplier.getAsLong();
        while (valueIterator.hasNext()) {
            ValueElement<T> valueElement = valueIterator.next();
            if (valueElement.isExpired(currentDateTs)) {
                valueIterator.remove();
            }
        }
    }

    public void invalidateAll() {
        cache.replaceAll((key, value) -> new ValueElement<>(value.getValue(), -1));
    }

    public static class ValueElement<T> {

        private final T value;

        private final long expirationTsInMs;

        public ValueElement(T value, long expirationTsInMs) {
            this.value = value;
            this.expirationTsInMs = expirationTsInMs;
        }

        public T getValue() {
            return value;
        }

        public boolean isExpired(long currentTsInMs) {
            return currentTsInMs > expirationTsInMs;
        }

    }
}
