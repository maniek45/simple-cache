package com.example.cache;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import io.dropwizard.testing.DropwizardTestSupport;
import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class CacheExpirationIntegrationTest {

    private static final String KEY_PARAM = "key";

    private static final String KEY_PATH_PARAM = "{" + KEY_PARAM + "}";

    private DropwizardTestSupport<CacheConfiguration> support;

    @BeforeClass
    public static void initStatic() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.basePath = "/cache";
    }

    @After
    public void stopServer() {
        support.after();
    }

    @Test
    public void givenElementInCacheIsPresentWhenGetNotExpiredElementBeforeElementEvictedThenFoundInCache() {
        // Given
        long expirationTimeoutInMs = 1000L;
        long autoEvictionIntervalInMs = 3000L;
        startServer(expirationTimeoutInMs, autoEvictionIntervalInMs);

        String elementKey = generateRandomKey();
        String elementValue = "value";
        given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Response getResponse = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(getResponse.getStatusCode()).isEqualTo(200);
        assertThat(getResponse.asString()).isEqualTo(elementValue);
    }

    @Test
    public void givenElementInCacheIsPresentWhenGetExpiredElementBeforeElementEvictedThenFoundInCache() throws
            Exception {
        // Given
        long expirationTimeoutInMs = 1000L;
        long autoEvictionIntervalInMs = 3000L;
        startServer(expirationTimeoutInMs, autoEvictionIntervalInMs);

        String elementKey = generateRandomKey();
        String elementValue = "value";
        given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Thread.sleep(expirationTimeoutInMs + 500L);
        Response getResponse = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(getResponse.getStatusCode()).isEqualTo(404);
    }

    @Test
    public void givenElementInCacheIsPresentWhenGetExpiredAfterElementEvictedThenNotFoundInCache() throws Exception {
        // Given
        long expirationTimeoutInMs = 1000L;
        long autoEvictionIntervalInMs = 3000L;
        startServer(expirationTimeoutInMs, autoEvictionIntervalInMs);

        String elementKey = generateRandomKey();
        String elementValue = "value";
        given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Thread.sleep(autoEvictionIntervalInMs + 500L);
        Response getResponse = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(getResponse.getStatusCode()).isEqualTo(404);
    }

    @Test
    public void givenElementInCacheIsPresentWhenGetNotExpiredAfterElementEvictedThenFoundInCache() throws Exception {
        // Given
        long expirationTimeoutInMs = 3000L;
        long autoEvictionIntervalInMs = 1000L;
        startServer(expirationTimeoutInMs, autoEvictionIntervalInMs);

        String elementKey = generateRandomKey();
        String elementValue = "value";
        given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Thread.sleep(2 * autoEvictionIntervalInMs);
        Response getResponse = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(getResponse.getStatusCode()).isEqualTo(200);
    }

    @Test
    public void givenElementInCacheIsPresentWhenGetNotExpiredAfterElementEvictedThenNotFoundInCache() throws Exception {
        // Given
        long expirationTimeoutInMs = 3000L;
        long autoEvictionIntervalInMs = 1000L;
        startServer(expirationTimeoutInMs, autoEvictionIntervalInMs);

        String elementKey = generateRandomKey();
        String elementValue = "value";
        given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Thread.sleep(autoEvictionIntervalInMs + expirationTimeoutInMs);
        Response getResponse = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(getResponse.getStatusCode()).isEqualTo(404);
    }

    private void startServer(long expirationTimeoutInMs, long autoEvictionIntervalInMs) {
        support = new DropwizardTestSupport<>(CacheApplication.class, new CacheConfiguration(expirationTimeoutInMs,
                autoEvictionIntervalInMs));
        support.before();
        RestAssured.port = support.getLocalPort();
    }

    private String generateRandomKey() {
        return UUID.randomUUID().toString();
    }

}
