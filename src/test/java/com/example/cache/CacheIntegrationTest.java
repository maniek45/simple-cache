package com.example.cache;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.response.Response;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.BeforeClass;
import org.junit.ClassRule;
import org.junit.Test;

import java.util.UUID;

import static com.jayway.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class CacheIntegrationTest {

    @ClassRule
    public static final DropwizardAppRule<CacheConfiguration> RULE = new DropwizardAppRule<>(CacheApplication.class);

    private static final String KEY_PARAM = "key";

    private static final String KEY_PATH_PARAM = "{" + KEY_PARAM + "}";

    @BeforeClass
    public static void buildUriBuilder() {
        RestAssured.baseURI = "http://localhost";
        RestAssured.basePath = "/cache";
        RestAssured.port = RULE.getLocalPort();
    }

    @Test
    public void givenOneElementInCacheIsPresentWhenGetByKeyThenElementFoundInCache() {
        // Given
        String elementKey = generateRandomKey();
        String elementValue = "value";
        given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Response getResponse = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(getResponse.getStatusCode()).isEqualTo(200);
        assertThat(getResponse.asString()).isEqualTo(elementValue);
    }

    @Test
    public void givenElementInCacheIsMissingWhenGetByKeyThenNotFoundStatusCode() {
        // Given
        String elementKey = generateRandomKey();

        // When
        Response response = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(response.getStatusCode()).isEqualTo(404);
    }

    @Test
    public void givenElementInCacheIsMissingWhenPostElementThenCreatedStatusCodeAndElementFoundInCacheByLocation() {
        // Given
        String elementKey = generateRandomKey();
        String elementValue = "value";

        // When
        Response postResponse = given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post
                (KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(postResponse.getStatusCode()).isEqualTo(201);

        String location = postResponse.header("location");
        assertThat(location).isNotNull();

        String actualElementValue = given().when().get(location).then().statusCode(200).extract().asString();
        assertThat(actualElementValue).isEqualTo(elementValue);
    }

    @Test
    public void givenElementInCacheIsPresentWhenPostElementThenOkStatusCodeAndElementFoundInCache() {
        // Given
        String elementKey = generateRandomKey();
        given().pathParam(KEY_PARAM, elementKey).body("value").when().post(KEY_PATH_PARAM).then().statusCode(201);
        String elementValue = "new value";

        // When
        Response postResponse = given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post
                (KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(postResponse.getStatusCode()).isEqualTo(200);
        String actualElementValue = given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).then()
                .statusCode(200).extract().asString();
        assertThat(actualElementValue).isEqualTo(elementValue);
    }

    @Test
    public void givenElementInCacheIsPresentWhenPostNextElementThenBothElementsFoundInCache() {
        // Given
        String firstElementKey = generateRandomKey();
        String firstElementValue = "first value";
        given().pathParam(KEY_PARAM, firstElementKey).body(firstElementValue).when().post(KEY_PATH_PARAM).then()
                .statusCode(201);

        String secondElementKey = generateRandomKey();
        String secondElementValue = "second value";

        // When
        Response postResponse = given().pathParam(KEY_PARAM, secondElementKey).body(secondElementValue).when().post
                (KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(postResponse.getStatusCode()).isEqualTo(201);

        String firstActualElementValue = given().pathParam(KEY_PARAM, firstElementKey).when().get(KEY_PATH_PARAM)
                .then().statusCode(200).extract().asString();
        assertThat(firstActualElementValue).isEqualTo(firstElementValue);

        String secondActualElementValue = given().pathParam(KEY_PARAM, secondElementKey).when().get(KEY_PATH_PARAM)
                .then().statusCode(200).extract().asString();
        assertThat(secondActualElementValue).isEqualTo(secondElementValue);
    }

    @Test
    public void givenElementInCacheIsMissingWhenDeleteElementThenNotFoundStatusCode() {
        // Given
        String elementKey = generateRandomKey();

        // When
        Response deleteResponse = given().pathParam(KEY_PARAM, elementKey).when().delete(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(deleteResponse.getStatusCode()).isEqualTo(404);
    }

    @Test
    public void givenElementInCacheIsPresentWhenDeleteElementThenOkStatusCodeAndElementNotFoundInCache() {
        // Given
        String elementKey = generateRandomKey();
        String elementValue = "value";
        given().pathParam(KEY_PARAM, elementKey).body(elementValue).when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Response deleteResponse = given().pathParam(KEY_PARAM, elementKey).when().delete(KEY_PATH_PARAM).thenReturn();

        // Then
        assertThat(deleteResponse.getStatusCode()).isEqualTo(200);
        given().pathParam(KEY_PARAM, elementKey).when().get(KEY_PATH_PARAM).then().statusCode(404);
    }

    @Test
    public void givenSomeElementsInCacheArePresentWhenInvalidateCacheThenAllOfThemNotFoundInCache() {
        // Given
        String firstElementKey = generateRandomKey();
        given().pathParam(KEY_PARAM, firstElementKey).body("value").when().post(KEY_PATH_PARAM).then().statusCode(201);

        String secondElementKey = generateRandomKey();
        given().pathParam(KEY_PARAM, secondElementKey).body("value").when().post(KEY_PATH_PARAM).then().statusCode(201);

        // When
        Response deleteResponse = given().when().delete().thenReturn();

        // Then
        assertThat(deleteResponse.getStatusCode()).isEqualTo(200);
        given().pathParam(KEY_PARAM, firstElementKey).when().get(KEY_PATH_PARAM).then().statusCode(404);
        given().pathParam(KEY_PARAM, secondElementKey).when().get(KEY_PATH_PARAM).then().statusCode(404);
    }

    private String generateRandomKey() {
        return UUID.randomUUID().toString();
    }

}
