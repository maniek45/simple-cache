package com.example.cache.resource;

import com.example.cache.service.CacheService;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StringCacheResourceTest {

    private StringCacheResource cacheResource;

    private CacheService<String> cacheService;

    @Before
    public void init() {
        cacheService = mock(CacheService.class);
        cacheResource = new StringCacheResource(cacheService);
    }

    @Test
    public void shouldReadReturnNotFoundStatusCodeWhenCacheServiceGetReturnsNull() {
        // Given
        String key = "key";
        when(cacheService.get(key)).thenReturn(null);
        // When
        Response response = cacheResource.read(key);
        // Then
        assertThat(response.getStatus()).isEqualTo(404);
        assertThat(response.getEntity()).isNull();
    }

    @Test
    public void shouldReadReturnOkStatusCodeAndValueWhenCacheServiceGetReturnsValue() {
        // Given
        String key = "key";
        String expectedValue = "value";
        when(cacheService.get(key)).thenReturn(expectedValue);
        // When
        Response response = cacheResource.read(key);
        // Then
        assertThat(response.getStatus()).isEqualTo(200);
        assertThat(response.getEntity()).isEqualTo(expectedValue);
    }

    @Test
    public void shouldAddReturnCreatedStatusCodeAndLocationWhenCacheServicePutReturnsTrue() {
        // Given
        String key = "key";
        String expectedValue = "value";
        when(cacheService.put(key, expectedValue)).thenReturn(true);
        // When
        Response response = cacheResource.add(key, expectedValue);
        // Then
        assertThat(response.getStatus()).isEqualTo(201);
        assertThat(response.getHeaderString("location")).isEqualTo("cache/" + key);
    }

    @Test
    public void shouldAddReturnOkStatusCodeWhenCacheServicePutReturnsFalse() {
        // Given
        String key = "key";
        String expectedValue = "value";
        when(cacheService.put(key, expectedValue)).thenReturn(false);
        // When
        Response response = cacheResource.add(key, expectedValue);
        // Then
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldRemoveReturnNotFoundStatusCodeWhenCacheServiceDeleteReturnsFalse() {
        // Given
        String key = "key";
        when(cacheService.delete(key)).thenReturn(false);
        // When
        Response response = cacheResource.remove(key);
        // Then
        assertThat(response.getStatus()).isEqualTo(404);
    }

    @Test
    public void shouldRemoveReturnOkStatusCodeWhenCacheServiceDeleteReturnsTrue() {
        // Given
        String key = "key";
        when(cacheService.delete(key)).thenReturn(true);
        // When
        Response response = cacheResource.remove(key);
        // Then
        assertThat(response.getStatus()).isEqualTo(200);
    }

    @Test
    public void shouldInvalidateAllAlwaysReturnOkStatusCode() {
        // When
        Response response = cacheResource.invalidateAll();
        // Then
        assertThat(response.getStatus()).isEqualTo(200);
    }
}
