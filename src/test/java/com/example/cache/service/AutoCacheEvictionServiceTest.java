package com.example.cache.service;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;

public class AutoCacheEvictionServiceTest {

    private final long evictionIntervalInMs = 10;

    private EvictionService evictionService;

    private Supplier<ScheduledExecutorService> scheduledExecutorServiceSupplier;

    private AutoCacheEvictionService autoCacheEvictionService;

    @SuppressWarnings("unchecked")
    @Before
    public void init() {
        evictionService = mock(EvictionService.class);
        scheduledExecutorServiceSupplier = mock(Supplier.class);
        autoCacheEvictionService = new AutoCacheEvictionService(evictionService, evictionIntervalInMs,
                scheduledExecutorServiceSupplier);
    }

    @Test
    public void shouldScheduleEvictionServiceEvictWhenStartingNotStartedServiceYet() {
        // Given
        ScheduledExecutorService scheduledExecutorService = mock(ScheduledExecutorService.class);
        when(scheduledExecutorServiceSupplier.get()).thenReturn(scheduledExecutorService);
        // When
        autoCacheEvictionService.start();
        // Then
        ArgumentCaptor<Runnable> commandCaptor = ArgumentCaptor.forClass(Runnable.class);
        verify(scheduledExecutorService).scheduleAtFixedRate(commandCaptor.capture(), eq(evictionIntervalInMs), eq
                (evictionIntervalInMs), eq(TimeUnit.MILLISECONDS));
        commandCaptor.getValue().run();
        verify(evictionService).evict();
    }

    @Test
    public void shouldRaiseIllegalStateExceptionWhenStartingAlreadyStartedServiceAgain() {
        // Given
        ScheduledExecutorService scheduledExecutorService = mock(ScheduledExecutorService.class);
        when(scheduledExecutorServiceSupplier.get()).thenReturn(scheduledExecutorService);
        autoCacheEvictionService.start();
        IllegalStateException exception = null;
        // When
        try {
            autoCacheEvictionService.start();
        } catch (IllegalStateException e) {
            exception = e;
        }
        // Then
        assertThat(exception).isNotNull();
    }

    @Test
    public void shouldShutdownScheduledExecutorWhenStoppingNotStoppedServiceYet() {
        // Given
        ScheduledExecutorService scheduledExecutorService = mock(ScheduledExecutorService.class);
        when(scheduledExecutorServiceSupplier.get()).thenReturn(scheduledExecutorService);
        autoCacheEvictionService.start();
        // When
        autoCacheEvictionService.stop();
        // Then
        verify(scheduledExecutorService).shutdown();
    }

    @Test
    public void shouldSkipShutdownScheduledExecutorWhenStoppingAlreadyStoppedService() {
        // Given
        ScheduledExecutorService scheduledExecutorService = mock(ScheduledExecutorService.class);
        when(scheduledExecutorServiceSupplier.get()).thenReturn(scheduledExecutorService);
        autoCacheEvictionService.start();
        autoCacheEvictionService.stop();
        // When
        autoCacheEvictionService.stop();
        // Then
        verify(scheduledExecutorService).shutdown();
    }
}
