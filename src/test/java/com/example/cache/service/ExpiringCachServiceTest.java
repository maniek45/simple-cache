package com.example.cache.service;

import org.junit.Before;
import org.junit.Test;

import java.util.function.LongSupplier;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ExpiringCachServiceTest {

    private final long expirationTimeoutInMs = 10;

    private ExpiringCacheService<Object> cacheStorageService;

    private LongSupplier currentDateSupplier;

    @Before
    public void init() {
        currentDateSupplier = mock(LongSupplier.class);
        cacheStorageService = new ExpiringCacheService<>(expirationTimeoutInMs, currentDateSupplier);
    }

    @Test
    public void shouldGetNullValueWhenElementInCacheIsMissing() {
        // Given
        String key = "key";
        // When
        Object actualValue = cacheStorageService.get(key);
        // Then
        assertThat(actualValue).isNull();
    }

    @Test
    public void shouldGetElementWhenElementInCacheIsPresentAndIsNotExpired() {
        // Given
        String key = "key";
        Object expectedValue = "value";
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        cacheStorageService.put(key, expectedValue);
        when(currentDateSupplier.getAsLong()).thenReturn(expirationTimeoutInMs);
        // When
        Object actualValue = cacheStorageService.get(key);
        // Then
        assertThat(actualValue).isEqualTo(actualValue);
    }

    @Test
    public void shouldGetNullValueWhenElementInCacheIsPresentAndIsExpired() {
        // Given
        String key = "key";
        Object expectedValue = "value";
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        cacheStorageService.put(key, expectedValue);
        when(currentDateSupplier.getAsLong()).thenReturn(expirationTimeoutInMs + 1);
        // When
        Object actualValue = cacheStorageService.get(key);
        // Then
        assertThat(actualValue).isNull();
    }

    @Test
    public void shouldGetTwoElementsWhenBothElementsInCacheArePresent() {
        // Given
        String firstKey = "first key";
        String firstExpectedValue = "first value";
        cacheStorageService.put(firstKey, firstExpectedValue);
        String secondKey = "second key";
        String secondExpectedValue = "second value";
        cacheStorageService.put(secondKey, secondExpectedValue);
        // When
        Object firstActualValue = cacheStorageService.get(firstKey);
        Object secondActualValue = cacheStorageService.get(secondKey);
        // Then
        assertThat(firstActualValue).isEqualTo(firstExpectedValue);
        assertThat(secondActualValue).isEqualTo(secondExpectedValue);
    }

    @Test
    public void shouldPutElementReturnTrueWhenElementInCacheIsMissing() {
        // Given
        String key = "key";
        Object expectedValue = "value";
        // When
        boolean isNewElement = cacheStorageService.put(key, expectedValue);
        // Then
        assertThat(isNewElement).isEqualTo(true);
        assertThat(cacheStorageService.get(key)).isEqualTo(expectedValue);
    }

    @Test
    public void shouldPutElementReturnFalseWhenElementInCacheIsPresent() {
        // Given
        String key = "key";
        cacheStorageService.put(key, "value");
        Object expectedValue = "new value";
        // When
        boolean isNewElement = cacheStorageService.put(key, expectedValue);
        // Then
        assertThat(isNewElement).isEqualTo(false);
        assertThat(cacheStorageService.get(key)).isEqualTo(expectedValue);
    }

    @Test
    public void shouldDeleteElementReturnFalseWhenElementInCacheIsMissing() {
        // Given
        String key = "key";
        // When
        boolean isElementDeleted = cacheStorageService.delete(key);
        // Then
        assertThat(isElementDeleted).isEqualTo(false);
        assertThat(cacheStorageService.get(key)).isNull();
    }

    @Test
    public void shouldDeleteElementReturnTrueWhenElementInCacheIsPresent() {
        // Given
        String key = "key";
        cacheStorageService.put(key, "value");
        // When
        boolean isElementDeleted = cacheStorageService.delete(key);
        // Then
        assertThat(isElementDeleted).isEqualTo(true);
        assertThat(cacheStorageService.get(key)).isNull();
    }

    @Test
    public void shouldGetElementsWhenElementsInCacheAreEvictedBeforeExpirationTimeout() {
        // Given
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String firstKey = "first key";
        Object firstExpectedValue = "first value";
        cacheStorageService.put(firstKey, firstExpectedValue);
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String secondKey = "second key";
        Object secondExpectedValue = "second value";
        cacheStorageService.put(secondKey, secondExpectedValue);
        when(currentDateSupplier.getAsLong()).thenReturn(expirationTimeoutInMs);
        // When
        cacheStorageService.evict();
        // Then
        assertThat(cacheStorageService.get(firstKey)).isEqualTo(firstExpectedValue);
        assertThat(cacheStorageService.get(secondKey)).isEqualTo(secondExpectedValue);
    }

    @Test
    public void shouldGetNullValueWhenElementsInCacheAreEvictedAfterExpirationTimeout() {
        // Given
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String firstKey = "first key";
        cacheStorageService.put(firstKey, "first value");
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String secondKey = "second key";
        cacheStorageService.put(secondKey, "second value");
        when(currentDateSupplier.getAsLong()).thenReturn(expirationTimeoutInMs + 1);
        // When
        cacheStorageService.evict();
        // Then
        assertThat(cacheStorageService.get(firstKey)).isNull();
        assertThat(cacheStorageService.get(secondKey)).isNull();
    }

    @Test
    public void shouldGetNullValueWhenElementsInCacheAreInvalidatedBeforeExpirationTimeout() {
        // Given
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String firstKey = "first key";
        cacheStorageService.put(firstKey, "first value");
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String secondKey = "second key";
        cacheStorageService.put(secondKey, "second value");
        when(currentDateSupplier.getAsLong()).thenReturn(expirationTimeoutInMs);
        // When
        cacheStorageService.invalidateAll();
        // Then
        assertThat(cacheStorageService.get(firstKey)).isNull();
        assertThat(cacheStorageService.get(secondKey)).isNull();
    }

    @Test
    public void shouldGetNullValueWhenElementsInCacheAreInvalidatedAfterExpirationTimeout() {
        // Given
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String firstKey = "first key";
        cacheStorageService.put(firstKey, "first value");
        when(currentDateSupplier.getAsLong()).thenReturn(0L);
        String secondKey = "second key";
        cacheStorageService.put(secondKey, "second value");
        when(currentDateSupplier.getAsLong()).thenReturn(expirationTimeoutInMs + 1);
        // When
        cacheStorageService.invalidateAll();
        // Then
        assertThat(cacheStorageService.get(firstKey)).isNull();
        assertThat(cacheStorageService.get(secondKey)).isNull();
    }
}
